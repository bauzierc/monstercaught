'use strict';

// var a = 2;
// var b = '3';
//document.write(a+Number(b));
//Aula 03
// document.write((5>10) + '<br>');
// document.write((5<10) + '<br>');
// document.write((5==10) + '<br>');
// document.write( ((11>10) && (5<10))+ '<br>');
// document.write( ((11>10) || (5<10)) + '<br>');

// Array
// let lista= ['leite','água','banana'];
// document.write('lista ['+lista + ']<br>'); //Mostra todos os elementos
// document.write('Primeiro elemento ['+ lista[1] + ']<br>'); //Mostra o segundo elemento
// document.write('Removendo ultimo elemento [' );
// lista.pop(); //Remove Ultimo elemento
// document.write(lista+ ']<br>');

// lista.shift(); //Remove primeiro elemento
// document.write('Removendo primeiro elemento['+ lista +']<br>'); //Mostra o segundo elemento

// lista.unshift('feijão'); //adiciona no inicio
// document.write('Inserindo no inicio ['+ lista +']<br>'); //Mostra o segundo elemento

// lista.push('café'); //inserir no final
// document.write('Inserindo no final ['+ lista +']<br>'); //Mostra o segundo elemento

// lista.splice(1,2); //Remove varios elemetos 
// document.write('Remove varios elementos ['+ lista +']<br>'); //Mostra o segundo elemento
// Loop while e for
// If else break continue
// switch

// Aula 04
// Objeto 
//Forma 1
// let caneta= {
// 	cor: "preta",
// 	marca: "BIC"
// };

// document.write('Cor da caneta: '   + caneta.cor   + '<br>');
// document.write('Marca da caneta: ' + caneta.marca + '<br>');
// // Forma 2
// // let caneta= {};
// caneta.tipo= '05'; // Adicionando atributo
// document.write('Tipo da caneta: '  + caneta.tipo  + '<br>');

// //Forma 3
// // let caneta= {}; 
// caneta['serie']= 'prime'; // Adicionando atributo
// document.write('Serie da caneta: '  + caneta['serie'] + '<br>');

//Trabalhando com metodos 3 formas

// let caneta= {
// 	cor: "preta",
// 	minhaCor: function(){
// 		return 'Minha cor' + this.cor;
// 	}
// };

// document.write('Cor da caneta: '   + caneta.minhaCor()   + '<br>');

// //função externa 
// function quadrado( num){
// 	return num * num;
// };

// document.write(quadrado(3) +'<br>');
// // Expressão de função

// let soma = function( num1,num2){
// 	return num1 + num2;
// };
// document.write(soma(3,2) +'<br>');

// //Hero function
// let raiz = num => {
// 	return num;
// }

// document.write(raiz(9) +'<br>');

//**** Aula 5 ***

// Criar o canvas
var canvas = document.createElement('canvas');
var ctx = canvas.getContext('2d');
canvas.width = 512; //Largura
canvas.height = 480; //Altura
document.body.appendChild(canvas);

var sizeImagem = 32;
var setaCima = 38;
var setaBaixo = 40;

var setaLeft = 37;
var setaRight = 39;

var timeUpdade = 1000;
//Tratamento com bordas 
var boderRight = canvas.width - sizeImagem * 2; //sizeImagem + sizeArvore
var boderLeft = sizeImagem; //sizeImagem 
var boderTop = sizeImagem; //sizeImagem 
var boderDown = canvas.height - sizeImagem * 2;; //sizeImagem + sizeArvore

var endGame = false;

//Imagem de fundo 
var bgReady = false;
var bgImagem = new Image();
bgImagem.onload = function () {
	//Evento carregar imagem execulta essa função
	bgReady = true;
};
bgImagem.src = 'images/background.png';

//Imagem do heroi
var heroReady = false;
var heroImagem = new Image();
heroImagem.onload = function () {
	//Evento carregar imagem execulta essa função
	heroReady = true;
};
heroImagem.src = 'images/mario.png';

//Imagem do mosto
var monsterReady = false;
var monsterImagem = new Image();
monsterImagem.onload = function () {
	//Evento carregar imagem execulta essa função
	monsterReady = true;
};
monsterImagem.src = 'images/star.png';

//Objeto do jogo

var hero = {
	speed: 256, //movimento em pixels por segundo 
	x: 0,
	y: 0
};
var monster = {
	x: 0,
	y: 0
};
var monsterCaught = 0;

// Eventos  controle doe teclado
var keysDown = {};

window.addEventListener('keydown', function (e) {
	// console.log(e);
	keysDown[e.keyCode] = true;
}, false);

window.addEventListener('keyup', function (e) {
	//console.log(e);
	delete keysDown[e.keyCode];
}, false);

// Reseta o jogo quando o jogador pega o monstro
var reset = function reset() {
	hero.x = canvas.width / 2; //Centro da tela y
	hero.y = canvas.height / 2; //centro da tela x

	monster.x = sizeImagem + Math.random() * (canvas.width - 64); //64 afastar do borda
	monster.y = sizeImagem + Math.random() * (canvas.height - 64); //64 afastar do borda
};

//Atualiza os objetos do jogo 
var updade = function updade(modifier) {

	// console.log('Posisão do heroi : ('+ hero. x +',' +hero. y + ')');

	if (setaCima in keysDown && hero.y >= boderTop) {
		//Presionado a seta para cima
		hero.y -= hero.speed * modifier;
	}

	if (setaBaixo in keysDown && !(hero.y >= boderDown)) {
		//Presionado a seta para baixo
		hero.y += hero.speed * modifier;
	}

	if (setaLeft in keysDown && hero.x >= boderLeft) {
		//Presionado a seta para esquerda
		hero.x -= hero.speed * modifier;
	}

	if (setaRight in keysDown && !(hero.x >= boderRight)) {
		//Presionado a seta para direita
		hero.x += hero.speed * modifier;
	}

	// Os personagens se enconstaram ?
	if (hero.x <= monster.x + sizeImagem && monster.x <= hero.x + sizeImagem && hero.y <= monster.y + sizeImagem && monster.y <= hero.y + sizeImagem) {
		monsterCaught++;
		if (monsterCaught == 2) {
			endGame = true;
		}
		reset();
	}
};

//rederiza tudo 
var render = function render() {
	if (bgReady) {
		ctx.drawImage(bgImagem, 0, 0);
	}

	if (heroReady) {
		ctx.drawImage(heroImagem, hero.x, hero.y);
	}

	if (monsterImagem) {
		ctx.drawImage(monsterImagem, monster.x, monster.y);
	}

	//Pontuação 
	ctx.fillStyle = 'rgb(250,250,250)';
	ctx.font = '24px Helvetica';
	ctx.textAling = 'left';
	ctx.tetxBaseline = 'top';
	if (!endGame) {
		ctx.fillText('Monstros pegos: ' + monsterCaught, 32, 32);
	} else {
		ctx.fillText('Fim do Jogo ', canvas.width / 2, canvas.height / 2);
	}
};

//Control o loop do jogo

var main = function main() {
	var now = Date.now();
	var delta = now - then;

	updade(delta / timeUpdade);
	render();
	then = now;
	if (!endGame) {
		//Executa mais breve possivel
		requestAnimationFrame(main);
	}
};

var w = window;
var requestAnimationFrame = w.requestAnimationFrame || w.webkitRequestAnimationFrame || w.msRequestAnimationFrame || w.mozRequestAnimationFrame;

var then = Date.now();
reset();
main();
// console.log(Date.now());
